#include "request.h"

#include <QtQml>
#include <QNetworkReply>

QNetworkAccessManager *Request::_manager = new QNetworkAccessManager;

Request::Request(QObject *parent) :
    QObject(parent),
    _status(Null),
    _BASE_API(QStringLiteral("https://haqton.qmob.solutions")),
    _JWT("Bearer eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE2MDU5MzQ1NjksImlhdCI6MTYwMjA0NjU2OSwiaXNzIjoiUW1vYiBTb2x1dGlvbnMiLCJ1c2VyIjp7InVzZXJuYW1lIjoibHVjYXNzb3V6YXVmcGFAZ21haWwuY29tIn19.PjyxSbGd0Sz0IrKfDp0hwEwS4Tn3fAdIcqiDTXvgnQIgsiPF1F6XzxDVSw3i9H4DDI6TdlbleFqmCnFZ3UWUgdFAFztvcRg3Z_7wRjaI46xtJM-fUvH_2gh1dAEpbewpKjO7UCfrdOx61EkIhMCsH4Rk9rfg1EYGjm_MEJ8OTITg6AMJ7CAM0M_X9z-EDbm2bLdt978jFcGF7cWZdUxl-jfvLgpY1jte7t-iI9F64unPvjSlP6H45Qbl49C4HpWlV2ifZ-CMN-UXUXaSpNnvgX4D9WrQKO59U6wYYG-bZUp5JtcBI5FI2S5SiUbDHD0-Y7lHsum9W6tcs-3tggvZPhMDa1JpnhrtA-PTUe3CBjVAU14YA7CtJHMmshYNYBBqPtlgWgOseXtHDSgoYIpfJ-Oj1X6WUy5CXRvwrh5dAEhWMPoUVGtyTU3TQL1ssDpJlNFUVcl0K3ViCQ5M1oRCbXgoFTGX_eqkTZr2Y1BQZKHsScJVvhGav9Cnu73TpI2v18q29IWSw4OJHz5XmyaAWeS16OKi5sYYg1YCKj9nlccX999KkK9-GDVZgWtPS7y572LLhF3jtAhRCl6hv46XO-np7gzr60-bEr_rcyI2fwJQDDs4sSeKSkQVkQB2ufyxMJBO58uTbUeowfDBXOYG5WzNbKbA3e3F5SghYIXqky4")
{
}

void Request::del(const QString path)
{
    setStatus(Loading);

    QUrl url(_BASE_API+path);
    QNetworkRequest request(url);
    request.setRawHeader(QByteArray("Authorization"), _JWT);
    request.setTransferTimeout(QNetworkRequest::DefaultTransferTimeoutConstant);

    QNetworkReply *reply = _manager->deleteResource(request);

    connect(reply, &QNetworkReply::finished, [=](){
        if (!reply->error()) {
            setResponse(reply->readAll());
            setStatus(Ready);
        } else {
            setResponse(reply->errorString());
            setStatus(Error);
        }
    });
}

void Request::get(const QString path)
{
    setStatus(Loading);

    QUrl url(_BASE_API+path);
    QNetworkRequest request(url);
    request.setRawHeader(QByteArray("Authorization"), _JWT);
    request.setTransferTimeout(QNetworkRequest::DefaultTransferTimeoutConstant);

    QNetworkReply *reply = _manager->get(request);

    connect(reply, &QNetworkReply::finished, [=]() {
        if (!reply->error()) {
            setResponse(reply->readAll());
            setStatus(Ready);
        } else {
            setResponse(reply->errorString());
            setStatus(Error);
        }
        reply->deleteLater();
    });
}

void Request::post(const QString path, QString data)
{
    setStatus(Loading);

    QUrl url(_BASE_API+path);
    QNetworkRequest request(url);
    request.setRawHeader(QByteArray("Authorization"), _JWT);
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
    request.setTransferTimeout(QNetworkRequest::DefaultTransferTimeoutConstant);

    QNetworkReply *reply = _manager->post(request, data.toUtf8());

    connect(reply, &QNetworkReply::finished, [=](){
        if (!reply->error()) {
            setResponse(reply->readAll());
            setStatus(Ready);
        } else {
            setResponse(reply->errorString());
            setStatus(Error);
        }
    });
}

void Request::put(const QString path, QString data)
{
    setStatus(Loading);

    QUrl url(_BASE_API+path);
    QNetworkRequest request(url);
    request.setRawHeader(QByteArray("Authorization"), _JWT);
    request.setTransferTimeout(QNetworkRequest::DefaultTransferTimeoutConstant);

    QNetworkReply *reply = _manager->put(request, data.toUtf8());

    connect(reply, &QNetworkReply::finished, [=](){
        if (!reply->error()) {
            setResponse(reply->readAll());
            setStatus(Ready);
        } else {
            setResponse(reply->errorString());
            setStatus(Error);
        }
    });
}

QString Request::getResponse() const
{
    return _response;
}

void Request::setResponse(const QString &response)
{
    if (_response != response) {
        _response = response;
        emit responseChanged(response);
    }
}

Request::Status Request::getStatus() const
{
    return _status;
}

void Request::setStatus(const Status &status)
{
    if (_status != status) {
        _status = status;
        emit statusChanged(status);
    }
}
