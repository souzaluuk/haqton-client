import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

Dialog {
    id: dialog

    title: qsTr("Criar uma partida")
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    parent: Overlay.overlay
    focus: true
    modal: true
    clip: true
    standardButtons: Dialog.Ok | Dialog.Cancel

    ColumnLayout {
        width: parent.width
        spacing: internal.margins / 2
        Label {
            text: qsTr("Nome")
            Layout.fillWidth: true
        }
        TextField {
            id: description
            placeholderText: qsTr("Nome da partida")
            Layout.fillWidth: true
            focus: true
        }
    }
    onAccepted: {
        if (description.text) {
            let data = {
                "description": description.text,
                "nickname": appRoot.nickname,
                "email": appRoot.email
            }
            Core.game.createMatch(data)
        } else {
            toolTipMessage.show(qsTr("Informe um nome válido"))
        }
    }

    Connections {
        enabled: stackView.currentItem === initialItemStack
        target: Core.game

        function onCurrentMatchCreated() {
            stackView.push(creatorAwaitingPlayers)
        }
    }

    Component {
        id: creatorAwaitingPlayers
        AwaitingPlayers {}
    }
}
