#ifndef REQUEST_H
#define REQUEST_H

#include <QObject>
#include <QNetworkAccessManager>

class Request : public QObject
{
    Q_OBJECT

public:
    explicit Request(QObject *parent = nullptr);
    enum Status {
        Null,
        Ready,
        Loading,
        Error
    };
    Q_ENUM(Status)
    Status getStatus() const;
    QString getResponse() const;

    void del(const QString path);
    void get(const QString path);
    void post(const QString path, const QString data);
    void put(const QString path, const QString data);

signals:
    void statusChanged(const Status &status);
    void responseChanged(const QString &response);

private:
    Status _status;
    QString _response;

    static QNetworkAccessManager *_manager;
    const QString _BASE_API;
    const QByteArray _JWT;

    void setStatus(const Status &status);
    void setResponse(const QString &response);
};

#endif // REQUEST_H
