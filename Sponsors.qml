import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Page {
    id: pageSponsors

    header: ToolBar {
        Material.background: internal.colorBackground
        Material.foreground: "white"
        contentItem: RowLayout {
            GameToolButton {
                text: Icons.faArrowLeft
                onClicked: stackView.pop()
            }
            Label {
                text: qsTr("Patrocinadores")
                elide: Label.ElideRight
                font {
                    bold: true
                }
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }
    }

    contentItem: ListView {
        ScrollIndicator.vertical: ScrollIndicator {}
        model: ListModel {
            ListElement {
                label: "openSUSE"
                link: "https://www.opensuse.org/"
                iconSource: "qrc:/assets/images/opensuse.png"
            }
            ListElement {
                label: "Toradex"
                link: "https://www.toradex.com/"
                iconSource: "qrc:/assets/images/toradex.png"
            }
            ListElement {
                label: "B2Open Systems"
                link: "https://www.b2open.com/"
                iconSource: "qrc:/assets/images/b2open.png"
            }
        }
        delegate: Item {
            width: parent.width
            height: content.height
            Button {
                id: content
                width: parent.width - internal.margins
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: Qt.openUrlExternally(link)
                contentItem: RowLayout {
                    Label {
                        text: label
                        Layout.fillWidth: true
                        elide: Label.ElideRight
                        font {
                            bold: true
                        }
                    }
                    Image {
                        source: iconSource
                        fillMode: Image.PreserveAspectFit
                        sourceSize.height: 40
                        smooth: true
                    }
                }
                Material.background: "white"
            }
        }
    }
}
