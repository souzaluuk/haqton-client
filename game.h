#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QLoggingCategory>

#include "request.h"
#include "match.h"

class Game : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Match * currentMatch READ getCurrentMatch NOTIFY currentMatchChanged)
    Q_PROPERTY(QJsonArray matchesWaitingPlayers READ getMatchesWaitingPlayers WRITE setMatchesWaitingPlayers NOTIFY matchesWaitingPlayersChanged)
    Q_PROPERTY(Request::Status statusRequest READ getStatusRequest NOTIFY statusRequestChanged)
    Q_PROPERTY(QJsonObject currentPlayer READ getCurrentPlayer NOTIFY currentPlayerChanged)
    Q_PROPERTY(QJsonObject currentQuestion READ getCurrentQuestion WRITE setCurrentQuestion NOTIFY currentQuestionChanged)

public:
    static Game *instance();

    QJsonArray getMatchesWaitingPlayers() const;
    void setMatchesWaitingPlayers(const QJsonArray &array);

    Request::Status getStatusRequest() const;

    Q_INVOKABLE void loadMatchesWaitingPlayers();
    Q_INVOKABLE void addPlayerToMatch(const QJsonObject &match, const QJsonObject &player);
    Q_INVOKABLE void createMatch(const QJsonObject &data);
    Q_INVOKABLE void deleteCurrentMatch();
    Q_INVOKABLE void updatePlayerFromCurrentMatch(const int playerId, const QJsonObject &data);
    Q_INVOKABLE void startCurrentMatch();
    Q_INVOKABLE void finishMatch();
    Q_INVOKABLE void loadRandomQuestion();
    Q_INVOKABLE void processPlayerAnswer(int questionId, int playerOption);

    Match *getCurrentMatch() const;

    QJsonObject getCurrentPlayer() const;

    QJsonObject getCurrentQuestion() const;

Q_SIGNALS:
    void matchesWaitingPlayersChanged(const QJsonArray &matchesWaitingPlayers);
    void statusRequestChanged(const Request::Status &statusRequest);
    void currentMatchChanged(const Match * currentMatch);
    void currentPlayerChanged(const QJsonObject currentPlayer);
    void currentQuestionChanged(const QJsonObject currentQuestion);

    void requestError(const QString &error);
    void currentMatchCreated();
    void currentMatchDeleted();
    void currentPlayerAddedToMatch();
    void currentPlayerDeletedFromMatch();

private:
    explicit Game(QObject *parent = nullptr);
    static Game *_instance;

    QJsonArray matchesWaitingPlayers;
    QJsonObject currentPlayer;
    QJsonObject currentQuestion;
    Match *currentMatch;

    Request *_request;
    Request::Status _statusRequest;

    void setCurrentMatch(Match *value);
    void setCurrentPlayer(const QJsonObject &value);
    void setCurrentQuestion(const QJsonObject &value);
    void setStatusRequest(const Request::Status &statusRequest);
};

#endif // GAME_H
