import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "FontAwesome"

Dialog {
    title: qsTr("Informe os dados do jogador")
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    parent: Overlay.overlay
    focus: true
    modal: true
    clip: true
    standardButtons: Dialog.Save | Dialog.Reset

    onAccepted: {
        appRoot.nickname = nicknameField.text.trim()
        appRoot.email = emailField.text.trim()
    }
    onRejected: {
        nicknameField.text = appRoot.nickname
        emailField.text = appRoot.email
    }
    onReset: {
        nicknameField.text = ""
        emailField.text = ""
        appRoot.nickname = nicknameField.text.trim()
        appRoot.email = emailField.text.trim()
        close()
    }

    ColumnLayout {
        width: parent.width
        spacing: internal.margins / 2
        Label {
            text: qsTr("Nome:")
            Layout.fillWidth: true
        }
        TextField {
            id: nicknameField
            text: appRoot.nickname
            placeholderText: qsTr("Nome do jogador")
            selectByMouse: true
            focus: true
            Layout.fillWidth: true
        }
        Label {
            width: parent.width
            text: qsTr("E-mail:")
            Layout.fillWidth: true
        }
        TextField {
            id: emailField
            text: appRoot.email
            placeholderText: qsTr("E-mail do jogador")
            selectByMouse: true
            focus: true
            inputMethodHints: Qt.ImhEmailCharactersOnly
            validator: RegExpValidator {
                regExp: /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
            }
            Layout.fillWidth: true
        }
    }
}
