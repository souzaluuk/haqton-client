import QtQuick.Controls 2.15

import "FontAwesome"

ToolButton {
    font {
        family: FontAwesome.solid
        styleName: "Solid"
    }
}
