#ifndef MATCH_H
#define MATCH_H

#include <QObject>
#include <QJsonArray>
#include <QJsonObject>

class Match : public QObject {
    Q_OBJECT
    Q_PROPERTY(int id READ getId WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString creator_name READ getCreatorName WRITE setCreatorName NOTIFY creatorNameChanged)
    Q_PROPERTY(QString creator_email READ getCreatorEmail WRITE setCreatorEmail NOTIFY creatorEmailChanged)
    Q_PROPERTY(bool status READ getStatus WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(int topic READ getTopic WRITE setTopic NOTIFY topicChanged)
    Q_PROPERTY(QJsonArray match_players READ getMatchPlayers WRITE setMatchPlayers NOTIFY matchPlayersChanged)

public:
    Match(const QJsonObject matchJson, QObject *parent = nullptr);

    int getId() const;
    void setId(int value);

    QString getDescription() const;
    void setDescription(const QString &value);

    QString getCreatorName() const;
    void setCreatorName(const QString &value);

    QString getCreatorEmail() const;
    void setCreatorEmail(const QString &value);

    bool getStatus() const;
    void setStatus(bool value);

    int getTopic() const;
    void setTopic(int value);

    QJsonArray getMatchPlayers() const;
    void setMatchPlayers(const QJsonArray &value);

    QJsonObject findPlayer(const int id);

Q_SIGNALS:
    void idChanged(int id);
    void descriptionChanged(QString &description);
    void creatorNameChanged(QString &creator_name);
    void creatorEmailChanged(QString &creator_email);
    void statusChanged(bool status);
    void topicChanged(int topic);
    void matchPlayersChanged(QJsonArray &match_players);

private:
    int id;
    QString description;
    QString creatorName;
    QString creatorEmail;
    bool status;
    int topic;
    QJsonArray matchPlayers;
};

#endif // MATCH_H
