#include "match.h"

Match::Match(const QJsonObject matchJson, QObject *parent)
    : QObject(parent),
      id(matchJson["id"].toInt()),
      description(matchJson["description"].toString()),
      creatorName(matchJson["creator_name"].toString()),
      creatorEmail(matchJson["creator_email"].toString()),
      status(matchJson["status"].toBool()),
      topic(matchJson["topic"].toInt()),
      matchPlayers(matchJson["match_players"].toArray())
{
}

int Match::getId() const
{
    return id;
}

void Match::setId(int value)
{
    if (id != value) {
        id = value;
        emit idChanged(id);
    }
}

QString Match::getDescription() const
{
    return description;
}

void Match::setDescription(const QString &value)
{
    if (description != value) {
        description = value;
        emit descriptionChanged(description);
    }
}

QString Match::getCreatorName() const
{
    return creatorName;
}

void Match::setCreatorName(const QString &value)
{
    if (creatorName != value) {
        creatorName = value;
        emit creatorNameChanged(creatorName);
    }
}

QString Match::getCreatorEmail() const
{
    return creatorEmail;
}

void Match::setCreatorEmail(const QString &value)
{
    if (creatorEmail != value) {
        creatorEmail = value;
        emit creatorEmailChanged(creatorEmail);
    }
}

bool Match::getStatus() const
{
    return status;
}

void Match::setStatus(bool value)
{
    if (status != value) {
        status = value;
        emit statusChanged(status);
    }
}

int Match::getTopic() const
{
    return topic;
}

void Match::setTopic(int value)
{
    if (topic != value) {
        topic = value;
        emit topicChanged(topic);
    }
}

QJsonArray Match::getMatchPlayers() const
{
    return matchPlayers;
}

void Match::setMatchPlayers(const QJsonArray &value)
{
    if (matchPlayers != value) {
        matchPlayers = value;
        emit matchPlayersChanged(matchPlayers);
    }
}

QJsonObject Match::findPlayer(const int id)
{
    foreach (QJsonValue player, matchPlayers) {
        if (player["id"].toInt() == id) {
            return player.toObject();
        }
    }
    return QJsonObject();
}
