import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import "FontAwesome"

RoundButton {
    Material.background: "white"
    font {
        family: FontAwesome.solid
        styleName: "Solid"
        pixelSize: 18
    }
}
