
/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

ApplicationWindow {
    id: appRoot

    property string nickname
    property string email

    width: 380
    height: 620
    visible: true
    title: Qt.application.name

    function playerIsValid() {
        return !!appRoot.nickname && !!appRoot.email
    }

    Connections {
        target: Core.game
        function onRequestError(error) {
            toolTipMessage.show(error)
        }
    }

    FontLoader {
        id: gameFont
        source: "assets/fonts/PocketMonk-15ze.ttf"
    }
    Audio {
        id: audioGame
        source: "assets/audio/Magic-Clock-Shop_Looping.mp3"
        loops: Audio.Infinite
        autoPlay: true
    }

    QtObject {
        id: internal
        property int margins: 10
        property alias colorBackground: mainRect.color
    }

    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: "#44a8e4"

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation {
                to: "#44a8e4"
                duration: 5000
            }
            ColorAnimation {
                to: "#8bc740"
                duration: 5000
            }
            ColorAnimation {
                to: "#f3d034"
                duration: 5000
            }
            ColorAnimation {
                to: "#f79154"
                duration: 5000
            }
            ColorAnimation {
                to: "#f3d034"
                duration: 5000
            }
            ColorAnimation {
                to: "#8bc740"
                duration: 5000
            }
        }
    }

    StackView {
        id: stackView
        anchors {
            fill: parent
        }

        initialItem: Item {
            id: initialItemStack
            anchors {
                right: parent.right
                left: parent.left
                margins: internal.margins
            }
            ColumnLayout {
                width: parent.width
                anchors {
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -haqtonLabel.height
                }
                Label {
                    id: haqtonLabel
                    Layout.topMargin: 8 * internal.margins
                    font {
                        family: gameFont.name
                        pixelSize: 64
                    }
                    Layout.alignment: Qt.AlignHCenter
                    color: "white"
                    text: "HaQton!"
                    SequentialAnimation on scale {
                        loops: Animation.Infinite
                        PropertyAnimation {
                            to: 0.25
                            duration: 1500
                            easing.type: Easing.OutElastic
                        }
                        PropertyAnimation {
                            to: 1
                            duration: 1500
                            easing.type: Easing.OutElastic
                        }
                        PauseAnimation {
                            duration: 10000
                        }
                    }
                    SequentialAnimation on rotation {
                        loops: Animation.Infinite
                        PropertyAnimation {
                            to: -45
                            duration: 1000
                            easing.type: Easing.OutElastic
                        }
                        PropertyAnimation {
                            to: 45
                            duration: 1000
                            easing.type: Easing.OutElastic
                        }
                        PropertyAnimation {
                            to: 0
                            duration: 1000
                            easing.type: Easing.OutElastic
                        }
                        PauseAnimation {
                            duration: 10000
                        }
                    }
                }
                GridLayout {
                    columns: 2
                    columnSpacing: internal.margins / 2
                    rowSpacing: internal.margins / 2
                    Layout.fillWidth: true
                    Layout.fillHeight: false
                    Repeater {
                        model: [{
                                "icon": Icons.faPlusCircle,
                                "text": "Criar uma partida",
                                "action": "criarPartida"
                            }, {
                                "icon": Icons.faGamepad,
                                "text": "Participar de uma partida",
                                "action": "participarPartida"
                            }, {
                                "icon": Icons.faDiceD20,
                                "text": "Sobre o HaQton",
                                "action": "sobreHaqton"
                            }, {
                                "icon": Icons.faMedal,
                                "text": "Patrocinadores",
                                "action": "patrocinadoresHaqton"
                            }]
                        GameButton {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            iconLabel {
                                text: modelData.icon
                                color: mainRect.color
                            }
                            text: modelData.text
                            onClicked: {
                                const actionValid = ["criarPartida", "participarPartida"].includes(
                                                      modelData.action)

                                if (actionValid && !playerIsValid()) {
                                    dadosJogadorConnection.action = modelData.action
                                    warningCreateUser.open()
                                } else {
                                    switch (modelData.action) {
                                    case "criarPartida":
                                        criarPartidaDialog.open()
                                        break
                                    case "participarPartida":
                                        stackView.push(matchesWaitingPlayers)
                                        break
                                    case "sobreHaqton":
                                        stackView.push(about)
                                        break
                                    case "patrocinadoresHaqton":
                                        stackView.push(sponsors)
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        ColumnLayout {
            anchors {
                top: parent.top
                right: parent.right
            }
            GameRoundButton {
                text: playerIsValid() ? Icons.faUserCheck : Icons.faUserPlus
                Material.foreground: mainRect.color
                onClicked: dadosJogadorDialog.open()
            }
            GameRoundButton {
                text: audioGame.muted ? Icons.faVolumeMute : Icons.faVolumeUp
                Material.foreground: mainRect.color
                onClicked: {
                    if (audioGame.muted) {
                        audioGame.play()
                        audioGame.muted = false
                    } else {
                        audioGame.pause()
                        audioGame.muted = true
                    }
                }
            }
        }
    }

    Connections {
        id: dadosJogadorConnection
        property string action

        target: dadosJogadorDialog
        enabled: !!dadosJogadorConnection.action

        function onAccepted() {
            switch (dadosJogadorConnection.action) {
            case "criarPartida":
                criarPartidaDialog.open()
                break
            case "participarPartida":
                stackView.push(matchesWaitingPlayers)
                break
            }
            dadosJogadorConnection.action = ""
        }
    }

    ToolTip {
        id: toolTipMessage
        timeout: 1500
        delay: 100
        x: (parent.width - width) / 2
        y: parent.height - 1.5 * height
    }

    Component {
        id: matchesWaitingPlayers
        MatchesWaitingPlayers {}
    }

    Component {
        id: about
        About {}
    }

    Component {
        id: sponsors
        Sponsors {}
    }

    CreateMatchDialog {
        id: criarPartidaDialog
    }

    UserDataDialog {
        id: dadosJogadorDialog
    }

    Dialog {
        id: warningCreateUser
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        parent: Overlay.overlay
        modal: true
        standardButtons: Dialog.Ok | Dialog.Cancel
        contentData: Label {
            text: qsTr("Crie um usuário para ter acesso")
        }
        onAccepted: {
            dadosJogadorDialog.open()
        }
    }

    GameBusy {
        running: Core.game.statusRequest === Request.Loading
    }
}
