import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Page {
    id: pageRunningMatch

    property var match_players: Core.game.currentMatch.match_players
    property int creatorId: match_players.length > 0 ? match_players[0].id : -1
    property var new_question: Core.game.currentQuestion
    property bool match_finished: false

    function allResponded() {
        return match_players.every(function (player) {
            return player.player_option !== -1
        })
    }
    function resetPlayerResponses() {
        Core.game.currentMatch.match_players = match_players.map(
                    function (player) {
                        player["player_option"] = -1
                        return player
                    })
    }
    function updatePlayers(new_answer) {
        Core.game.currentMatch.match_players = match_players.map(
                    function (player) {
                        if (player.id === new_answer.player_id) {
                            player.player_option = new_answer.player_option
                            player.score += player.player_option
                                    === new_question.right_option ? 1 : 0
                        }
                        return player
                    })
    }

    function playersSort() {
        return Core.game.currentMatch.match_players.sort(
                    function (player_a, player_b) {
                        if (player_a.score > player_b.score) {
                            return -1
                        }
                        return player_a.score < player_b.score ? 1 : 0
                    })
    }

    header: ToolBar {
        Material.background: internal.colorBackground
        Material.foreground: "white"
        contentItem: RowLayout {
            Label {
                text: Core.game.currentMatch.description + " - " + Core.game.currentMatch.id
                elide: Label.ElideRight
                font {
                    bold: true
                }
                Layout.alignment: Qt.AlignHCenter
            }
        }
    }
    contentItem: Flickable {
        id: flickable
        contentWidth: flickable.width
        contentHeight: contentFlickable.height
        ScrollIndicator.vertical: ScrollIndicator {}
        ColumnLayout {
            id: contentFlickable
            width: flickable.width
            spacing: internal.margins
            ColumnLayout {
                Layout.margins: internal.margins
                Label {
                    text: new_question.description
                    wrapMode: Label.WordWrap
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                }
                ButtonGroup {
                    id: radioGroup
                    exclusive: true
                }
                Repeater {
                    model: new_question.question_options
                    delegate: RadioDelegate {
                        text: modelData.description
                        enabled: Core.game.currentPlayer.player_option === -1
                        onClicked: {
                            Core.game.processPlayerAnswer(new_question.id,
                                                          index)
                            const isCorrect = index === Core.game.currentQuestion.right_option
                            toolTipResult.colorResult = isCorrect ? "green" : "red"
                            const msg = isCorrect ? qsTr(
                                                        "Opção correta") : qsTr(
                                                        "Opção incorreta")
                            toolTipResult.show(msg)
                        }
                        ButtonGroup.group: radioGroup
                        Layout.fillWidth: true
                    }
                }
            }
            Label {
                text: qsTr("Participantes:")
                Layout.alignment: Qt.AlignHCenter
            }
            Repeater {
                model: playersSort()
                delegate: ItemDelegate {
                    Layout.fillWidth: true
                    contentItem: RowLayout {
                        spacing: internal.margins
                        ColumnLayout {
                            Layout.fillWidth: true
                            Label {
                                Layout.fillWidth: true
                                text: modelData.id
                                elide: Label.ElideRight
                                font {
                                    bold: true
                                }
                            }
                            Label {
                                Layout.fillWidth: true
                                text: modelData.player_name
                                elide: Label.ElideRight
                                font {
                                    bold: true
                                }
                            }
                            Label {
                                Layout.fillWidth: true
                                text: modelData.player_email
                                elide: Label.ElideRight
                            }
                        }
                        Label {
                            text: modelData.score
                            font.bold: true
                        }
                        Label {
                            property var playerOption: modelData.player_option

                            text: playerOption === -1 ? Icons.faUserClock : Icons.faUserCheck
                            ToolTip.text: playerOption === -1 ? qsTr("Ainda não respondeu") : qsTr(
                                                                    "Já respondeu")
                            ToolTip.visible: mouseAreaLabel.containsMouse
                            MouseArea {
                                id: mouseAreaLabel
                                anchors.fill: parent
                                hoverEnabled: true
                            }
                            font {
                                family: FontAwesome.solid
                                styleName: "Solid"
                                pixelSize: 18
                            }
                        }
                    }
                }
            }
        }
    }

    footer: ToolBar {
        visible: Core.game.currentPlayer.id === creatorId || match_finished
        Material.background: "white"
        Material.foreground: Material.accent
        contentItem: RowLayout {
            Button {
                Layout.fillWidth: true
                Material.background: "transparent"
                text: qsTr("Ver resultado")
                visible: match_finished
                onClicked: stackView.push(resultMatch)
            }
            Button {
                Layout.fillWidth: true
                Material.background: "transparent"
                text: qsTr("Encerrar Partida")
                visible: !match_finished
                onClicked: Core.game.finishMatch()
            }
            Button {
                Layout.fillWidth: true
                Material.background: "transparent"
                text: qsTr("Próxima pergunta")
                enabled: allResponded()
                onClicked: Core.game.loadRandomQuestion()
                visible: !match_finished
            }
        }
    }

    ToolTip {
        property color colorResult

        id: toolTipResult
        timeout: 1000
        Material.background: colorResult
        x: (parent.width - width) / 2
        y: parent.height - 1.5 * height
    }

    Connections {
        target: Core.messagingController
        enabled: stackView.currentItem === pageRunningMatch

        function onNewMessage(message) {
            const response = JSON.parse(message)
            switch (response.message) {
            case "new_question":
                resetPlayerResponses()
                Core.game.currentQuestion = response.data
                break
            case "new_answer":
                updatePlayers(response.data)
                break
            case "match_finished":
                match_finished = true
                toolTipMessage.show(qsTr("Partida encerrada"))
                break
            }
        }
    }

    Component {
        id: resultMatch
        ResultMatch {}
    }

    Component.onCompleted: {
        resetPlayerResponses()
        Core.messagingController.setTopic(Core.game.currentMatch.topic)
    }
}
