import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Page {
    id: pageMatchesWaitingPlayers

    StackView.onActivated: {
        Core.game.loadMatchesWaitingPlayers()
        Core.messagingController.setTopic("matches")
    }

    Connections {
        target: Core.messagingController
        enabled: stackView.currentItem === pageMatchesWaitingPlayers

        function onNewMessage(message) {
            const response = JSON.parse(message)
            switch (response.message) {
            case "matches_update":
                Core.game.matchesWaitingPlayers = response.data
                break
            }
        }
    }

    function filterMatches(nameSearch) {
        return Core.game.matchesWaitingPlayers.sort(
                    function (match_a, match_b) {
                        const desc_a = match_a.description.toLowerCase()
                        const desc_b = match_b.description.toLowerCase()
                        if (desc_a < desc_b) {
                            return -1
                        }
                        return desc_a > desc_b ? 1 : 0
                    }).filter(function (value) {
                        let title = value.description.toLowerCase()
                        title += " - "
                        title += value.id.toString()

                        return title.includes(nameSearch.toLowerCase())
                    })
    }

    header: ToolBar {
        Material.background: internal.colorBackground
        Material.foreground: "white"
        contentItem: RowLayout {
            GameToolButton {
                text: Icons.faArrowLeft
                onClicked: stackView.pop()
            }
            Label {
                id: labelTitle
                text: qsTr("Partidas Disponíveis")
                elide: Label.ElideRight
                visible: !searchInput.visible
                font {
                    bold: true
                }
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            TextField {
                id: searchInput
                width: labelTitle.width
                visible: false
                focus: visible
                placeholderText: qsTr("Nome da partida")
                onVisibleChanged: {
                    text = ""
                }
                Layout.fillWidth: true
            }
            GameToolButton {
                text: searchInput.visible ? Icons.faTimes : Icons.faSearch
                onClicked: searchInput.visible = !searchInput.visible
            }
        }
    }

    contentItem: ListView {
        id: listView
        model: filterMatches(searchInput.displayText)
        delegate: Item {
            width: listView.width
            height: content.height
            Button {
                id: content
                width: listView.width - internal.margins
                Material.background: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: {
                    const player = {
                        "nickname": appRoot.nickname,
                        "email": appRoot.email
                    }
                    Core.game.addPlayerToMatch(modelData, player)
                }
                contentItem: RowLayout {
                    Layout.fillWidth: true
                    ColumnLayout {
                        Label {
                            Layout.fillWidth: true
                            text: modelData.description + " - " + modelData.id
                            elide: Label.ElideRight
                            font {
                                bold: true
                            }
                        }
                        Label {
                            Layout.fillWidth: true
                            text: "Autor: " + modelData.creator_name
                            elide: Label.ElideRight
                        }
                        Label {
                            Layout.fillWidth: true
                            text: "E-mail: " + modelData.creator_email
                            elide: Label.ElideRight
                        }
                    }
                    Label {
                        Material.foreground: mainRect.color
                        text: Icons.faClock
                        rightPadding: 10
                        leftPadding: 10
                        font {
                            family: FontAwesome.solid
                            styleName: "Solid"
                            pixelSize: 18
                        }
                        ToolTip.text: qsTr("Aguardando jogadores")
                        ToolTip.visible: mouseAreaLabel.containsMouse
                        MouseArea {
                            id: mouseAreaLabel
                            anchors.fill: parent
                            hoverEnabled: true
                        }
                    }
                }
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }

    Connections {
        enabled: stackView.currentItem === pageMatchesWaitingPlayers
        target: Core.game

        function onCurrentPlayerAddedToMatch() {
            stackView.push(playerWaiting)
        }
    }

    Component {
        id: playerWaiting
        AwaitingPlayers {}
    }
}
