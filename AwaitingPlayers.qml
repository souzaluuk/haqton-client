import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Page {
    id: pageCreatorAwaitingPlayers

    property var match_players: Core.game.currentMatch.match_players
    property int creatorId: match_players.length > 0 ? match_players[0].id : -1

    function allApprovedPlayers() {
        return match_players.every(function (player) {
            return player.approved
        })
    }

    function isLastPlayer() {
        return match_players.length === 1
    }

    header: ToolBar {
        Material.background: internal.colorBackground
        Material.foreground: "white"
        contentItem: RowLayout {
            Label {
                text: qsTr("Aguardando jogadores - ") + Core.game.currentMatch.description
                elide: Label.ElideRight
                font {
                    bold: true
                }
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            GameToolButton {
                text: isLastPlayer() ? Icons.faTrash : Icons.faSignOutAlt
                onClicked: {
                    if (isLastPlayer()) {
                        Core.game.deleteCurrentMatch()
                    } else {
                        const data = {
                            "approved": false
                        }
                        Core.game.updatePlayerFromCurrentMatch(
                                    Core.game.currentPlayer.id, data)
                    }
                }
            }
        }
    }

    contentItem: ListView {
        id: listView
        model: match_players
        delegate: Item {
            width: listView.width
            height: content.height
            Button {
                Material.background: "white"
                id: content
                width: listView.width - internal.margins
                anchors.horizontalCenter: parent.horizontalCenter
                contentItem: RowLayout {
                    spacing: internal.margins
                    ColumnLayout {
                        Layout.fillWidth: true
                        Label {
                            Layout.fillWidth: true
                            text: modelData.id
                            elide: Label.ElideRight
                            font {
                                bold: true
                            }
                        }
                        Label {
                            Layout.fillWidth: true
                            text: modelData.player_name
                            elide: Label.ElideRight
                            font {
                                bold: true
                            }
                        }
                        Label {
                            Layout.fillWidth: true
                            text: modelData.player_email
                            elide: Label.ElideRight
                        }
                    }
                    Label {
                        property string toolTipMessage: modelData.approved ? qsTr("Participação aprovada") : qsTr("Aguardando aprovação")
                        property string iconSource: modelData.approved ? Icons.faUserCheck : Icons.faUserClock

                        Material.foreground: modelData.approved ? Material.Green : Material.Grey
                        visible: Core.game.currentPlayer.id !== creatorId
                                 || modelData.approved
                        text: iconSource
                        font {
                            family: FontAwesome.solid
                            styleName: "Solid"
                            pixelSize: 18
                        }
                        ToolTip.text: toolTipMessage
                        ToolTip.visible: mouseAreaLabel.containsMouse
                        MouseArea {
                            id: mouseAreaLabel
                            anchors.fill: parent
                            hoverEnabled: true
                        }
                    }
                    RowLayout {
                        visible: Core.game.currentPlayer.id === creatorId
                                 && !modelData.approved
                        GameToolButton {
                            Material.foreground: hovered ? Material.Green : Material.Grey
                            Layout.fillHeight: true
                            text: Icons.faUserCheck
                            ToolTip.text: qsTr("Aprovar participação")
                            ToolTip.visible: hovered
                            font {
                                pixelSize: 18
                            }
                            onClicked: {
                                const data = {
                                    "approved": true
                                }
                                Core.game.updatePlayerFromCurrentMatch(
                                            modelData.id, data)
                            }
                        }
                        GameToolButton {
                            Material.foreground: hovered ? Material.Red : Material.Grey
                            Layout.fillHeight: true
                            text: Icons.faUserTimes
                            ToolTip.text: qsTr("Reprovar participação")
                            ToolTip.visible: hovered
                            font {
                                pixelSize: 18
                            }
                            onClicked: {
                                const data = {
                                    "approved": false
                                }
                                Core.game.updatePlayerFromCurrentMatch(
                                            modelData.id, data)
                            }
                        }
                    }
                }
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }
    footer: ToolBar {
        visible: Core.game.currentPlayer.id === creatorId
        Material.background: "white"
        Material.foreground: Material.accent
        contentItem: RowLayout {
            Button {
                Layout.fillWidth: true
                enabled: allApprovedPlayers()
                text: qsTr("Iniciar Partida")
                Material.background: "transparent"
                onClicked: {
                    Core.game.startCurrentMatch()
                }
            }
        }
    }

    Component {
        id: runningMatch
        RunningMatch {}
    }

    Connections {
        target: Core.messagingController
        enabled: stackView.currentItem === pageCreatorAwaitingPlayers

        function onNewMessage(message) {
            const response = JSON.parse(message)
            switch (response.message) {
            case "players_update":
                Core.game.currentMatch.match_players = response.data
                break
            case "new_question":
                Core.game.currentQuestion = response.data
                stackView.push(runningMatch)
                break
            }
        }
    }

    Connections {
        target: Core.game
        enabled: stackView.currentItem === pageCreatorAwaitingPlayers

        function onCurrentMatchDeleted() {
            stackView.pop()
        }
        function onCurrentPlayerDeletedFromMatch() {
            stackView.pop()
        }
    }

    Component.onCompleted: {
        Core.messagingController.setTopic(Core.game.currentMatch.topic)
    }
}
