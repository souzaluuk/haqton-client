#include "game.h"

#include <QtQml>

Q_LOGGING_CATEGORY(game, "solutions.qmob.haqton.game");

Game *Game::_instance = nullptr;

Game::Game(QObject *parent) :
    QObject(parent),
    currentMatch(new Match(QJsonObject()))
{
    qmlRegisterInterface<Game>("Game", 1);
}

QJsonObject Game::getCurrentQuestion() const
{
    return currentQuestion;
}

void Game::setCurrentQuestion(const QJsonObject &value)
{
    if (currentQuestion != value) {
        currentQuestion = value;
        emit currentQuestionChanged(currentQuestion);
    }
}

QJsonObject Game::getCurrentPlayer() const
{
    return currentPlayer;
}

void Game::setCurrentPlayer(const QJsonObject &value)
{
    if (currentPlayer != value)
    {
        currentPlayer = value;
        emit currentPlayerChanged(currentPlayer);
    }
}

Match *Game::getCurrentMatch() const
{
    return currentMatch;
}

void Game::setCurrentMatch(Match *value)
{
    if (currentMatch != value) {
        currentMatch = value;

        connect(currentMatch, &Match::matchPlayersChanged, [=](){
            int currentPlayerId = currentPlayer.value("id").toInt();
            QJsonObject findPlayer = currentMatch->findPlayer(currentPlayerId);

            if (!findPlayer.empty())
            {
                setCurrentPlayer(findPlayer);
                qCDebug(game) << "setCurrentMatch update currentPlayer";
            } else
            {
                emit currentPlayerDeletedFromMatch();
                qCDebug(game) << "setCurrentMatch: currentPlayer removed";
            }
        });

        emit currentMatchChanged(currentMatch);
    }
}

void Game::setStatusRequest(const Request::Status &statusRequest)
{
    if (_statusRequest != statusRequest)
    {
        _statusRequest = statusRequest;
        emit statusRequestChanged(_statusRequest);
    }
}

Request::Status Game::getStatusRequest() const
{
    return _statusRequest;
}

QJsonArray Game::getMatchesWaitingPlayers() const
{
    return matchesWaitingPlayers;
}

void Game::setMatchesWaitingPlayers(const QJsonArray &array)
{
    if (matchesWaitingPlayers != array)
    {
        matchesWaitingPlayers = array;
        emit matchesWaitingPlayersChanged(matchesWaitingPlayers);
    }
}

void Game::loadMatchesWaitingPlayers()
{
    _request = new Request;
    connect(_request, &Request::statusChanged, [=](Request::Status status)
    {
        if (status == Request::Ready)
        {
            QJsonDocument jsonDocument = QJsonDocument::fromJson(_request->getResponse().toUtf8());
            setMatchesWaitingPlayers(jsonDocument.array());

            qCDebug(game) << "loadMatchesWaitingPlayers sucess:" << matchesWaitingPlayers.count() << "matches";
            _request->deleteLater();
        } else if (status == Request::Error)
        {
            emit requestError("Erro ao listar partidas");

            qCDebug(game) << "loadMatchesWaitingPlayers error:" << _request->getResponse();
            _request->deleteLater();
        }
        setStatusRequest(status);
    });
    _request->get("/matches");
}

void Game::addPlayerToMatch(const QJsonObject &match, const QJsonObject &player)
{
    QString matchId = QString::number(match.value("id").toInt());
    QJsonDocument playerDocument(player);

    _request = new Request;
    connect(_request, &Request::statusChanged, [=](Request::Status status)
    {
       if (status == Request::Ready)
       {
           Match *_currentMatch = new Match(match);
           QJsonDocument playersDocument = QJsonDocument::fromJson(_request->getResponse().toUtf8());
           _currentMatch->setMatchPlayers(playersDocument.array());

           setCurrentPlayer(_currentMatch->getMatchPlayers().last().toObject());
           setCurrentMatch(_currentMatch);
           emit currentPlayerAddedToMatch();

           qCDebug(game) << "addPlayerToMatch sucess:" << currentMatch->getId() << ":" << currentMatch->getDescription();
           _request->deleteLater();
       }
       else if (status == Request::Error)
       {
           emit requestError("Erro ao adicionar jogador");

           qCDebug(game) << "addPlayerToMatch fail:" << _request->getResponse();
           _request->deleteLater();
       }
       setStatusRequest(status);
    });
    _request->post("/matches/"+matchId+"/players", playerDocument.toJson());
}

void Game::createMatch(const QJsonObject &data)
{
    QJsonDocument dataDocument(data);
    _request = new Request;
    connect(_request, &Request::statusChanged, [=](Request::Status status) {
       if (status == Request::Ready)
       {
           QJsonDocument matchCreatedDocument = QJsonDocument::fromJson(_request->getResponse().toUtf8());
           Match *_currentMatch = new Match(matchCreatedDocument.object());

           setCurrentPlayer(_currentMatch->getMatchPlayers().first().toObject());
           setCurrentMatch(_currentMatch);
           emit currentMatchCreated();

           qCDebug(game) << "createMatch sucess:" << currentMatch->getId() << ":" << currentMatch->getDescription();
           _request->deleteLater();
       }
       else if (status == Request::Error)
       {
           emit requestError("Erro ao criar partida");

           qCDebug(game) << "createMatch fail:" << _request->getResponse();
           _request->deleteLater();
       }
       setStatusRequest(status);
    });
    _request->post("/matches", dataDocument.toJson());
}

void Game::deleteCurrentMatch()
{
    _request = new Request;
    connect(_request, &Request::statusChanged, [=](Request::Status status) {
        if (status == Request::Ready)
        {
            setCurrentPlayer(QJsonObject());
            setCurrentMatch(new Match(QJsonObject()));
            emit currentMatchDeleted();
            qCDebug(game) << "deleteCurrentMatch sucess";
            _request->deleteLater();
        }
        else if (status == Request::Error)
        {
            emit requestError("Erro ao deletar partida");
            qCDebug(game) << "deleteCurrentMatch fail" << _request->getResponse();
            _request->deleteLater();
        }
        setStatusRequest(status);
    });
    _request->del("/matches/"+QString::number(currentMatch->getId()));
}

void Game::updatePlayerFromCurrentMatch(const int playerId, const QJsonObject &data)
{
    _request = new Request;

    QString playerIdStr = QString::number(playerId);
    QString matchIdStr = QString::number(currentMatch->getId());
    QJsonDocument dataDocument(data);

    connect(_request, &Request::statusChanged, [=](Request::Status status) {
        if (status == Request::Ready)
        {
            QJsonDocument playersDocument = QJsonDocument::fromJson(_request->getResponse().toUtf8());
            if (playersDocument.isArray()) {
                currentMatch->setMatchPlayers(playersDocument.array());
                qCDebug(game) << "updatePlayerFromCurrentMatch sucess:" << currentMatch->getMatchPlayers().count() << "players";
            } else {
                emit requestError("Erro ao atualizar jogador");
                qCDebug(game) << "updatePlayerFromCurrentMatch response is not Array:" << _request->getResponse();
            }
            _request->deleteLater();
        }
        else if (status == Request::Error)
        {
            emit requestError("Erro ao atualizar jogador");
            qCDebug(game) << "updatePlayerFromCurrentMatch fail:" << _request->getResponse();
            _request->deleteLater();
        }
        setStatusRequest(status);
    });

    _request->put("/matches/"+matchIdStr+"/players/"+playerIdStr, dataDocument.toJson());
}

void Game::startCurrentMatch()
{
    _request = new Request;

    QString matchIdStr = QString::number(currentMatch->getId());
    QJsonObject data {{"status",1}}; // {"status":1} running
    QJsonDocument dataDocument(data);

    connect(_request, &Request::statusChanged, [=](Request::Status status) {
        if (status == Request::Ready)
        {
            QJsonDocument currentMatchDocument = QJsonDocument::fromJson(_request->getResponse().toUtf8());
            setCurrentMatch(new Match(currentMatchDocument.object()));

            qCDebug(game) << "startCurrentMatch sucess";
            _request->deleteLater();

            loadRandomQuestion();
        }
        else if (status == Request::Error)
        {
            emit requestError("Erro ao iniciar partida");
            qCDebug(game) << "startCurrentMatch fail:" << _request->getResponse();
            _request->deleteLater();
        }
        setStatusRequest(status);
    });

    _request->put("/matches/"+matchIdStr, dataDocument.toJson());
}

void Game::finishMatch()
{
    _request = new Request;

    QString matchIdStr = QString::number(currentMatch->getId());
    QJsonObject data {{"status",2}}; // {"status":2} finished
    QJsonDocument dataDocument(data);

    connect(_request, &Request::statusChanged, [=](Request::Status status) {
        if (status == Request::Ready)
        {
            QJsonDocument currentMatchDocument = QJsonDocument::fromJson(_request->getResponse().toUtf8());
            setCurrentMatch(new Match(currentMatchDocument.object()));

            qCDebug(game) << "finishMatch sucess";
            _request->deleteLater();
        }
        else if (status == Request::Error)
        {
            emit requestError("Erro ao iniciar partida");
            qCDebug(game) << "finishMatch fail:" << _request->getResponse();
            _request->deleteLater();
        }
        setStatusRequest(status);
    });

    _request->put("/matches/"+matchIdStr, dataDocument.toJson());
}

void Game::loadRandomQuestion()
{
    _request = new Request;

    QString matchIdStr = QString::number(currentMatch->getId());
    connect(_request, &Request::statusChanged, [=](Request::Status status){
        if (status == Request::Ready)
        {
            QJsonDocument newQuestionDocument = QJsonDocument::fromJson(_request->getResponse().toUtf8());
            setCurrentQuestion(newQuestionDocument.object());

            qCDebug(game) << "loadRandomQuestion sucess:" << newQuestionDocument.object().value("id").toInt();
            _request->deleteLater();
        }
        else if (status == Request::Error)
        {
            emit requestError("Erro ao obter nova questão");
            qCDebug(game) << "loadRandomQuestion fail:" << _request->getResponse();
            _request->deleteLater();
        }
        setStatusRequest(status);
    });
    _request->get("/matches/"+matchIdStr+"/random_question");

}

void Game::processPlayerAnswer(int questionId, int playerOption)
{
    _request = new Request;

    QString matchIdStr = QString::number(currentMatch->getId());
    QString playerIdString = QString::number(currentPlayer.value("id").toInt());

    QJsonObject data {
        {"question_id", questionId},
        {"player_option", playerOption}
    };

    connect(_request, &Request::statusChanged, [=](Request::Status status){
        if (status == Request::Ready)
        {
            qCDebug(game) << "processPlayerAnswer sucess";
            _request->deleteLater();
        }
        else if (status == Request::Error)
        {
            emit requestError("Erro ao processar resposta");
            qCDebug(game) << "processPlayerAnswer fail:" << _request->getResponse();
            _request->deleteLater();
        }
        setStatusRequest(status);
    });

    qDebug() << QJsonDocument(data).toJson();
    _request->post("/matches/"+matchIdStr+"/players/"+playerIdString+"/answer", QJsonDocument(data).toJson());
}

Game *Game::instance()
{
    if (!_instance) {
        _instance = new Game;
    }
    return _instance;
}
