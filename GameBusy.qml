import QtQuick.Controls 2.15

Popup {
    property bool running: false

    id: popup
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    modal: true
    parent: Overlay.overlay
    closePolicy: Popup.NoAutoClose
    contentItem: BusyIndicator {
        id: busyIndicator
        running: popup.running
    }
    onRunningChanged: {
        if (running) {
            popup.open()
        } else {
            popup.close()
        }
    }
}
