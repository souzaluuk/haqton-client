import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Page {
    id: pageAbout

    header: ToolBar {
        Material.background: internal.colorBackground
        Material.foreground: "white"
        contentItem: RowLayout {
            GameToolButton {
                text: Icons.faArrowLeft
                onClicked: stackView.pop()
            }
            Label {
                text: qsTr("Sobre o HaQton")
                elide: Label.ElideRight
                font {
                    bold: true
                }
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            GameToolButton {
                text: Icons.faExternalLinkSquareAlt
                onClicked: Qt.openUrlExternally("https://br.qtcon.org/")
            }
        }
    }
    contentItem: Flickable {
        id: flickable
        contentWidth: flickable.width
        contentHeight: contentFlickable.height
        ScrollIndicator.vertical: ScrollIndicator {}
        ColumnLayout {
            id: contentFlickable
            width: flickable.width
            spacing: internal.margins
            Label {
                text: "HaQton - QtCon Brasil 2020"
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignHCenter
                font {
                    bold: true
                    pointSize: 14
                }
                Layout.fillWidth: true
                Layout.topMargin: internal.margins
                Layout.leftMargin: internal.margins
                Layout.rightMargin: internal.margins
            }
            Label {
                text: "Competição de Programação Qt"
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignHCenter
                font {
                    bold: true
                    pointSize: 13
                }
                Layout.fillWidth: true
                Layout.leftMargin: internal.margins
                Layout.rightMargin: internal.margins
                Material.foreground: Material.Green
            }
            Label {
                text: "O HaQton tem como objetivo promover o uso do Qt na comunidade Brasileira e Latino-Americana através de uma competição de programação. Nesta competição, as equipes inscritas irão desenvolver uma aplicação Qt que requer o uso de funcionalidades relacionadas a UIs, multimídia, comunicação em rede via APIs RESTful e comunicação via barramentos de mensagens. Com isso, esperamos que novos talentos passem a integrar o ecossistema Qt brasileiro e latino-americano, através de discussões no canal Qt Brasil no Telegram, participação como prováveis palestrantes em edições futuras da QtCon Brasil, colaborações em projetos tais como o KDE, etc."
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignHCenter
                font {
                    pointSize: 12
                }
                Layout.fillWidth: true
                Layout.leftMargin: internal.margins
                Layout.rightMargin: internal.margins
                Layout.bottomMargin: internal.margins
                Material.foreground: Material.Grey
            }
        }
    }
}
