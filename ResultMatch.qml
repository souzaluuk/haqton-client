import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Page {
    id: pageResultMatch

    property var players: playersSort()

    function playersSort() {
        return Core.game.currentMatch.match_players.sort(
                    function (player_a, player_b) {
                        if (player_a.score > player_b.score) {
                            return -1
                        }
                        return player_a.score < player_b.score ? 1 : 0
                    })
    }

    header: ToolBar {
        Material.background: internal.colorBackground
        Material.foreground: "white"
        RowLayout {
            anchors.fill: parent
            Label {
                text: qsTr("Resultado da partida")
                elide: Label.ElideRight
                font {
                    bold: true
                }
                Layout.alignment: Qt.AlignCenter
            }
        }
    }
    contentItem: ListView {
        id: listView
        model: pageResultMatch.players
        delegate: Item {
            width: listView.width
            height: content.height
            Button {
                Material.background: "white"
                id: content
                width: listView.width - internal.margins
                anchors.horizontalCenter: parent.horizontalCenter

                contentItem: RowLayout {
                    spacing: internal.margins
                    ColumnLayout {
                        Layout.fillWidth: true
                        Label {
                            Layout.fillWidth: true
                            text: modelData.id
                            elide: Label.ElideRight
                            font {
                                bold: true
                            }
                        }
                        Label {
                            Layout.fillWidth: true
                            text: modelData.player_name
                            elide: Label.ElideRight
                            font {
                                bold: true
                            }
                        }
                        Label {
                            Layout.fillWidth: true
                            text: modelData.player_email
                            elide: Label.ElideRight
                        }
                    }
                    Label {
                        text: modelData.score
                        font.bold: true
                    }
                    Label {
                        Material.foreground: modelData.score === pageResultMatch.players[0].score ? Material.Green : Material.Red
                        text: modelData.score
                              === pageResultMatch.players[0].score ? Icons.faTrophy : Icons.faTired
                        font {
                            family: FontAwesome.solid
                            styleName: "Solid"
                            pixelSize: 18
                        }
                    }
                }
            }
        }
    }
    footer: ToolBar {
        Material.background: "white"
        Material.foreground: Material.accent
        contentItem: RowLayout {
            Button {
                Layout.fillWidth: true
                Material.background: "transparent"
                text: qsTr("Retornar")
                onClicked: stackView.pop(null)
            }
        }
    }
}
